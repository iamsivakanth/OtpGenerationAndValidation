<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:if test="${msg == 'Success'}">
		<b style="color: green;">${msg}</b>
	</c:if>
	<c:if test="${msg == 'Failed'}">
		<b style="color: red;">${msg}</b>
	</c:if>
</body>
</html>