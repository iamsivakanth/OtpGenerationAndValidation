package com.example.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OtpAthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(OtpAthenticationApplication.class, args);
	}
}