package com.example.main.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.example.otp.OtpGenerator;

@Component
public class MyMailSender {

	@Autowired
	JavaMailSender sender;

	public String sendEmail() {

		SimpleMailMessage msg = new SimpleMailMessage();

		String otp = OtpGenerator.getOtp();

		msg.setFrom("From@gmail.com");
		msg.setTo("To@gmail.com");
		msg.setText("You must confirm your identity using this one-time pass code : " + otp);
		msg.setSubject("OTP");

		new Thread(() -> sender.send(msg)).start();
		System.out.println("Otp sent");
		return otp;
	}
}