package com.example.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.main.service.IService;

@Controller
@RequestMapping(value = "/api")
public class OtpController {

	@Autowired
	IService service;

	@GetMapping(value = "/index")
	public String getIndex() {
		return "index";
	}

	@GetMapping(value = "/forgot")
	public String forgot() {

		service.otpSender();
		return "forgot";
	}

	@PostMapping(value = "/validate")
	public String otpValidate(@RequestParam String otp, Model model) {
		
		if (service.otpValidator(otp)) {
			model.addAttribute("msg", "Success");
			return "result";
		} else {
			model.addAttribute("msg", "Failed");
			return "result";
		}
	}
}