package com.example.main.service;

public interface IService {

	void otpSender();
	boolean otpValidator(String otp);
}