package com.example.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.main.mail.MyMailSender;

@Service
public class ServiceImpl implements IService {

	String otp = null;

	@Autowired
	MyMailSender mailSender;
	
	@Override
	public void otpSender() {
		otp = mailSender.sendEmail();
	}

	@Override
	public boolean otpValidator(String otp) {

		if (this.otp.equals(otp))
			return true;
		else
			return false;
	}
}