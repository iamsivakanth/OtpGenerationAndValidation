package com.example.otp;

import java.util.Random;

public class OtpGenerator {

	public static String getOtp() {
		return String.format("%06d", new Random().nextInt(999999));
	}
}